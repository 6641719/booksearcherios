//
//  Service.swift
//  BookSearcherApp
//
//  Created by Amir on 09.04.2022.
//

import Foundation

protocol BookManagerDelegate {
    func updateBooksList(_ bookManager: BookManager, bookList: [Book])
}

struct BookManager {
    
    let BASE_URL = "https://www.googleapis.com/books/v1/volumes?q="
    var delegate: BookManagerDelegate?
    
    func parse(query: String) {
        
        let decoder = JSONDecoder()
        
        guard let url = URL(string: "\(BASE_URL)\(query)") else {return}
        
        let session = URLSession(configuration: .default)
        
        let task = session.dataTask(with: url) { data, response, error in
            
            if let error = error {
                print("Failed to fetch data with error: ", error.localizedDescription)
            }
            
            guard let data = data else {return}
            
            do {
                let decodeData = try decoder.decode(BooksData.self, from: data)
                
                var booksList: [Book] = []
                
                for bookData in decodeData.items {
                    let oneBook = bookData.volumeInfo
                    let title = oneBook.title ?? ""
                    let authors = oneBook.authors ?? []
                    var thumbnail = ""
                    if let bookLink = oneBook.imageLinks {
                        thumbnail = bookLink.smallThumbnail ?? ""
                    }
                    
                    let parsedBook = Book(title: title, authors: authors, thumbnail: thumbnail)
                    booksList.append(parsedBook)
                }
                self.delegate?.updateBooksList(self, bookList: booksList)
                
            } catch let error {
                print("Failed to create json with error: ", error.localizedDescription)
            }
            
        }
        task.resume()
    }
}
