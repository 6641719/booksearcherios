//
//  Book.swift
//  BookSearcherApp
//
//  Created by Amir on 09.04.2022.
//

import Foundation

struct Book {
    
    let title: String
    let authors: [String]
    let thumbnail: String
    
}
