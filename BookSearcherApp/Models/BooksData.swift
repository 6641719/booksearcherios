//
//  BooksData.swift
//  BookSearcherApp
//
//  Created by Amir on 09.04.2022.
//

import Foundation

struct BooksData: Codable {
    let totalItems: Int
    let items: [Item]
}

struct Item: Codable {
    let volumeInfo: VolumeInfo
}

struct VolumeInfo: Codable {
    let title: String?
    let authors: [String]?
    let imageLinks: ImageLinks?
}

struct ImageLinks: Codable {
    let smallThumbnail: String?
}
