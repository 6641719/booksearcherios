//
//  TableViewController.swift
//  BookSearcherApp
//
//  Created by Amir on 09.04.2022.
//

import UIKit

class TableViewController: UIViewController {
    let tableView = UITableView()
    
    var bookManager = BookManager()
    
    var searchedList: [Book] = []
    
    let searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.showsCancelButton = true
        searchBar.sizeToFit()
        searchBar.setShowsCancelButton(false, animated: false)
        return searchBar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
}

extension TableViewController {
    private func setupView() {
        view.addSubview(tableView)
        
        bookManager.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        searchBar.becomeFirstResponder()
        searchBar.delegate = self
        
        navigationItem.titleView = searchBar
    }
    
}

extension TableViewController:  UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        cell.frame.inset(by: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
        let bookInfo = searchedList[indexPath.row]
        cell.textLabel?.text = bookInfo.title
        cell.detailTextLabel?.text = ""
        for author in bookInfo.authors {
            if bookInfo.authors.count < 2{
                cell.detailTextLabel?.text! += author
                
            } else {
                cell.detailTextLabel?.text! += "\(author) "
            }
        }

        let img = UIImageView()
        if let url = URL(string: bookInfo.thumbnail) {
            if let data = try? Data(contentsOf: url) {
                    // Create Image and Update Image View
                    img.image = UIImage(data: data)
                }
        }
        cell.imageView?.image = img.image
    
        return cell
    }
    
    
}

extension TableViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchText = searchBar.text?.replacingOccurrences(of: " ", with: "") ?? ""
        bookManager.parse(query: searchText)
    }
    
}

extension TableViewController: BookManagerDelegate {
    func updateBooksList(_ bookManager: BookManager, bookList: [Book]) {
        DispatchQueue.main.async {
            self.searchedList = bookList
            self.tableView.reloadData()
        }
    }
    
    
}
